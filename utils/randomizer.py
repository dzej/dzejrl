from random import randint
from typing import Iterable, Dict


def random_choice_index(chances: Iterable[int]) -> int:
    random_chance = randint(1, sum(chances))

    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w

        if random_chance <= running_sum:
            return choice
        choice += 1


def random_choice_from_dict(choice_dict: Dict[any, int]) -> any:
    chances = list(choice_dict.values())
    choices = list(choice_dict.keys())

    return choices[random_choice_index(chances)]
