import tcod


COMMON_KEY_MAP = {
    'set_fullscreen': {'vk': tcod.KEY_ENTER, 'lalt': True},

    'up': {'vk': tcod.KEY_UP},
    'down': {'vk': tcod.KEY_DOWN},
    'left': {'vk': tcod.KEY_LEFT},
    'right': {'vk': tcod.KEY_RIGHT},

    'enter': {'vk': tcod.KEY_ENTER},
    'cancel': {'vk': tcod.KEY_ESCAPE},
}
