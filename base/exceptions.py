
class EngineError(Exception):
    pass


class EngineConfigError(EngineError):
    pass


class InvalidActionError(EngineError):
    pass


class InvalidInputError(EngineError):
    pass


class GameError(EngineError):
    pass


class GameViewError(GameError):
    pass


class GameMenuError(GameViewError):
    pass


class GameMapError(GameError):
    pass
