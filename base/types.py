from enum import IntEnum
from typing import Dict, Any, Tuple, Union


class Direction8(IntEnum):
    """
    Represents one of the 8 directions on single plane.
    4 cardinal and 4 diagonal.
    """
    STOP = 0
    N = 1
    NE = 2
    E = 3
    SE = 4
    S = 5
    SW = 6
    W = 7
    NW = 8

    def __bool__(self):
        return self != self.STOP

    def next(self):
        v = self.value
        if self == self.STOP:
            pass
        elif self < self.NW:
            v += 1
        else:
            v = self.N
        return Direction8(v)

    def prev(self):
        v = self.value
        if self == self.STOP:
            pass
        elif self > self.N:
            v -= 1
        else:
            v = self.NW
        return Direction8(v)


class ZPosition(IntEnum):
    BOTTOM = 0
    LOW = 1000
    NORMAL = 2000
    HIGH = 3000
    TOP = 10000


MAP_Z_PRIORITY = ZPosition.NORMAL
MAP_Z_FLOOR_THICKNESS = 100


class FieldZPriority(IntEnum):
    BOTTOM = 0

    LOW_OBJ = 3
    OBJ = 4
    HIGH_OBJ = 5

    CHARACTER = 8

    TOP = 10


# Typing custom types:

KeyMap = Dict[str, Dict[str, Any]]

Color = Tuple[int, int, int]

CharMap = Dict[int, Tuple[int, int]]

Character = Union[str, int]
