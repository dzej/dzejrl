from typing import List


def wrap_text(text: str, line_width: int) -> List[str]:
    """
    Returns list of lines of text divided to fit given line_width.
    """
    lines = []
    words = text.split(' ')
    line = ''

    for word in words:
        if len(line) + 1 + len(word) <= line_width:
            line += (' ' if line else '') + word
        else:
            if line:
                lines.append(line)
            line = word

    if line:
        lines.append(line)

    return lines
