
CHARS = {
    # single border lines
    'se-line': 218,
    'sw-line': 191,
    'ne-line': 192,
    'nw-line': 217,
    'we-line': 196,
    'ns-line': 179,

    # double border lines
    'we-db-line': 205,
    'ns-db-line': 186,
    'se-db-line': 201,
    'sw-db-line': 187,
    'ne-db-line': 200,
    'nw-db-line': 188,
}
