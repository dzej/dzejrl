from dzejrl.constants import CHARS


def clip_dim(x, y, width, height, clip_x, clip_y, clip_width, clip_height):
    """
    Return rect value clipped to fit in the console.
    """
    clip_x2 = clip_x + clip_width
    clip_y2 = clip_y + clip_height
    x2 = x + width
    y2 = y + height

    visible = (
        (clip_x <= x <= clip_x2 or clip_x <= x2 <= clip_x2)
        and
        (clip_y <= y <= clip_y2 or clip_y <= y2 <= clip_y2)
    )

    clipped = {
        'x': x if x >= clip_x else clip_x,
        'y': y if y >= clip_y else clip_y,
        'width': width if x2 <= clip_x2 else clip_x2 - x,
        'height': height if y2 <= clip_y2 else clip_y2 - y
    }

    if clipped['x'] > x:
        clipped['width'] -= clipped['x'] - x
    if clipped['y'] > y:
        clipped['height'] -= clipped['y'] - y

    return visible, clipped


def clip_dim_to_panel(x, y, width, height, panel):
    return clip_dim(x, y, width, height, 1, 1, panel.width-2, panel.height-2)


def pos_visible(x, y, clip_x, clip_y, clip_width, clip_height):
    return clip_x <= x <= clip_x + clip_width and clip_y <= y <= clip_y + clip_height


def draw_thick_rect(drawable, x, y, width, height, fg=None, bg=None):
    con = drawable.console

    for bx, by, bwidth, bheight, char in [
        (x + 1, y, width - 1, 1, CHARS['we-db-line']),
        (x + 1, y + height, width - 1, 1, CHARS['we-db-line']),
        (x, y + 1, 1, height - 1, CHARS['ns-db-line']),
        (x + width, y + 1, 1, height - 1, CHARS['ns-db-line'])
    ]:
        visible, clipped_dim = clip_dim_to_panel(bx, by, bwidth, bheight, drawable.renderable)

        if visible:
            con.draw_rect(**clipped_dim, ch=char, fg=fg, bg=bg)

    for cx, cy, char in [
        (x, y, chr(CHARS['se-db-line'])),
        (x + width, y, chr(CHARS['sw-db-line'])),
        (x, y + height, chr(CHARS['ne-db-line'])),
        (x + width, y + height, chr(CHARS['nw-db-line'])),
    ]:
        if pos_visible(cx, cy, 1, 1, drawable.renderable.width - 3, drawable.renderable.height - 3):
            con.print(cx, cy, char, fg=fg, bg=bg)
