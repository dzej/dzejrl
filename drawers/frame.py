from dzejrl.components.drawable import Drawable


def draw_frame(draw_obj: Drawable, entity: int) -> None:
    # Drawing:
    draw_obj.console.draw_frame(
        getattr(draw_obj, 'x', 0),
        getattr(draw_obj, 'y', 0),
        getattr(draw_obj, 'width', 10),
        getattr(draw_obj, 'height', 10),
        title=getattr(draw_obj, 'text', ''),
        clear=getattr(draw_obj, 'clear_frame', False),
        fg=getattr(draw_obj, 'fg_color', None),
        bg=getattr(draw_obj, 'bg_color', None)
    )

    # Updating panel z-map:
    panel_height_rng = range(draw_obj.panel.height)
    panel_width_rng = range(draw_obj.panel.width)

    for x_range, y_range in [
        [panel_width_rng, [0]],
        [panel_width_rng, [draw_obj.panel.height - 1]],
        [[0], panel_height_rng],
        [[draw_obj.panel.width - 1], panel_height_rng]
    ]:
        for x in x_range:
            for y in y_range:
                if draw_obj.panel.z_map[y][x] < draw_obj.z:
                    draw_obj.panel.z_map[y][x] = draw_obj.z
