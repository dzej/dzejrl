import time

from dzejrl.components.drawable import Drawable


def draw_fps(draw_obj: Drawable, entity: int) -> None:
    try:
        fps = getattr(draw_obj, 'fps')
    except AttributeError:
        # init:
        draw_obj.fps = {
            'start_time': time.time(),
            'counter': 0,
            'fps': 0
        }
        fps = draw_obj.fps

    now = time.time()
    fps['counter'] += 1
    if now - fps['start_time'] > 1.0:
        fps['fps'] = fps['counter']
        fps['counter'] = 0
        fps['start_time'] = time.time()

    content = f'FPS:{fps["fps"]}'

    for i in range(len(content)):
        draw_obj.panel.z_map[draw_obj.y][draw_obj.x + i] = draw_obj.z
        draw_obj.console.print(draw_obj.x, draw_obj.y, content)
