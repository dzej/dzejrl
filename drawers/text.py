from dzejrl.components.drawable import Drawable


def draw_text(draw_obj: Drawable, entity: int) -> None:
    x = getattr(draw_obj, 'x', 0)
    y = getattr(draw_obj, 'y', 0)
    z = getattr(draw_obj, 'z', 0)
    string = getattr(draw_obj, 'text', 'no content')
    width = getattr(draw_obj, 'width') or len(string)
    height = getattr(draw_obj, 'height') or 1

    # Drawing:
    draw_obj.console.print_box(
        x=x, y=y,
        width=width, height=height,
        string=string,
        fg=getattr(draw_obj, 'fg_color', None),
        bg=getattr(draw_obj, 'bg_color', None)
    )

    # Updating z-map:
    for z_y in range(y, y + height):
        for z_x in range(x, x + width):
            draw_obj.panel.z_map[z_y][z_x] = z
