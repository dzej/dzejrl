import tcod

from dzejrl.components.renderable import Panel
from dzejrl.processors.base import BaseProcessor


class RenderProcessor(BaseProcessor):

    def __init__(self, engine: 'dzejrl.engine.Engine') -> None:
        super().__init__(engine)
        self.console = engine.root_console
        self.screen_width, self.screen_height = engine.screen_width, engine.screen_height

    def process(self, *args, **kwargs) -> None:
        self.console.clear()

        for entity, panel in sorted(self.world.get_component(Panel), key=lambda t: t[1].panel_z):
            if panel.is_enabled:
                panel.console.blit(self.console, panel.x, panel.y, width=self.screen_width, height=self.screen_height)
                panel.console.clear()
                panel.reset_z_map()

        tcod.console_flush()
