import esper


class BaseProcessor(esper.Processor):

    def __init__(self, engine: 'dzejrl.engine.Engine') -> None:
        self.engine = engine
        self.game = engine.game

    def process(self, *args, **kwargs) -> None:
        raise NotImplementedError
