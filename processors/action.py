import tcod

from dzejrl.components.action import Action
from dzejrl.base.exceptions import InvalidActionError
from dzejrl.processors.base import BaseProcessor


class ActionProcessor(BaseProcessor):

    def __init__(self, engine) -> None:
        super().__init__(engine)

    def process(self, *args, **kwargs) -> None:
        for entity, action in self.world.get_component(Action):
            if action.name == 'exit_app':
                self.engine.exit_request = True

            elif action.name == 'set_fullscreen':
                tcod.console_set_fullscreen(action.params['set'])

            elif action.name == 'move':
                print(f'move {action.params["direction"]}')

            else:
                raise InvalidActionError(f'Action named {action.name} is not supported.')

            # TODO: FINISH THIS!

            self.world.delete_entity(entity)
