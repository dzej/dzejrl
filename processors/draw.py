from dzejrl.components.drawable import DrawChar, DrawObj
from dzejrl.processors.base import BaseProcessor


class DrawProcessor(BaseProcessor):

    def process(self, *args, **kwargs) -> None:

        for entity, draw_obj in self.world.get_component(DrawObj):
            if draw_obj.is_enabled and draw_obj.panel.is_enabled:
                draw_obj.draw(draw_obj, entity)

        for entity, draw_char in self.world.get_component(DrawChar):
            if draw_char.is_enabled and draw_char.panel.is_enabled:
                draw_char.draw(draw_char, entity)