import tcod

from dzejrl.processors.base import BaseProcessor


class InputProcessor(BaseProcessor):

    def __init__(self, engine: 'dzejrl.engine.Engine') -> None:
        super().__init__(engine)
        self.key = tcod.Key()
        self.mouse = tcod.Mouse()

    def process(self, *args, **kwargs) -> None:
        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS | tcod.EVENT_MOUSE, self.key, self.mouse)
        self.engine.game.process_input(self.key, self.mouse)
