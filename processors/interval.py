from dzejrl.components.interval_evt import IntervalEvent
from dzejrl.processors.base import BaseProcessor


class IntervalProcessor(BaseProcessor):

    def process(self, *args, **kwargs) -> None:
        for entity, interval_evt in self.engine.world.get_component(IntervalEvent):
            interval_evt.check_for_interval(self.engine, entity, interval_evt)
