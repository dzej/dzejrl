from dzejrl.processors.default import DrawProcessor, RenderProcessor, InputProcessor, ActionProcessor, IntervalProcessor
from dzejrl.processors.constants import ProcessorFlags


DEFAULT_PROCESSORS_MAP = (
    # (Flag, ProcessorCls, priority)
    (ProcessorFlags(ProcessorFlags.DRAW), DrawProcessor, 10),
    (ProcessorFlags(ProcessorFlags.RENDER), RenderProcessor, 9),
    (ProcessorFlags(ProcessorFlags.INPUT), InputProcessor, 8),
    (ProcessorFlags(ProcessorFlags.ACTION), ActionProcessor, 7),
    (ProcessorFlags(ProcessorFlags.INTERVAL), IntervalProcessor, 6),
)
