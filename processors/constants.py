from functools import reduce
import operator

from enum import Flag, auto


class ProcessorFlags(Flag):
    DRAW = auto()
    RENDER = auto()
    INPUT = auto()
    ACTION = auto()
    INTERVAL = auto()


DEFAULT_PROCESSORS = (
    ProcessorFlags.DRAW, ProcessorFlags.RENDER, ProcessorFlags.INPUT, ProcessorFlags.ACTION,
    ProcessorFlags.INTERVAL
)

DEFAULT_PROCESSORS_FLAG = reduce(operator.or_, DEFAULT_PROCESSORS)
