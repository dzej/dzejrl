
class Action:

    def __init__(self, name, params=None):
        self.name = name
        self.params = params if params else {}
