from typing import Optional, Union

from tcod import console_put_char_ex

from dzejrl.base.types import Color, ZPosition, Character
from dzejrl.components.renderable import Panel
from dzejrl.constants import COLORS

__all__ = ['Drawable', 'DrawChar', 'DrawObj']


class Drawable:

    def __init__(self, *args, **kwargs) -> None:
        self.panel = None
        self.console = None
        self.x = None
        self.y = None
        self.z = None


class DrawChar(Drawable):

    def __init__(
        self,
        panel: Panel,
        character: Character,
        x: int, y: int, z: int = ZPosition.NORMAL,
        is_enabled: bool = True,
        fg_color: Optional[Color] = None,
        bg_color: Optional[Color] = None,
        **options
    ) -> None:

        super().__init__()

        self.panel = panel
        self.console = self.panel.console
        self.x, self.y, self.z = x, y, z
        self.character = character
        self.is_enabled = is_enabled
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.options = options

    def draw(self, entity: int, *args, **kwargs) -> None:
        if (
            0 <= self.x < self.panel.width and
            0 <= self.y < self.panel.height and
            self.panel.z_map[self.y][self.x] <= self.z
        ):
            # We are using deprecated functions to be able to use custom font characters
            # which exceeds 255 range.
            fg_color = self.fg_color or COLORS['white']
            bg_color = self.bg_color or COLORS['black']
            console_put_char_ex(
                self.console, self.x, self.y, self.character,
                fore=fg_color, back=bg_color
            )
            self.panel.z_map[self.y][self.x] = self.z


class DrawObj(Drawable):

    def __init__(
        self,
        panel: Panel,
        draw_fn: callable,
        x: int, y: int, z: int = ZPosition.NORMAL,
        width: Optional[int] = None, height: Optional[int] = None,
        text: str = '',
        is_enabled: bool = True,
        fg_color: Optional[Color] = None,
        bg_color: Optional[Color] = None,
        **options
    ) -> None:

        super().__init__()

        self.panel = panel
        self.console = self.panel.console
        self.x, self.y, self.z = x, y, z
        self.width, self.height = width, height
        self.draw = draw_fn
        self.text = text
        self.is_enabled = is_enabled
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.options = options
