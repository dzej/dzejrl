from typing import Optional, Tuple

from tcod import tcod

from dzejrl.base.types import ZPosition


class Panel:

    def __init__(
        self,
        width: int, height: int,
        dest_x: int = 0, dest_y: int = 0,
        is_enabled: bool = True,
        title: str = '',
        border_fg: Optional[Tuple[int, int, int]] = None,
        panel_z: int = ZPosition.NORMAL
    ) -> None:

        self.width, self.height = width, height
        self.x, self.y = dest_x, dest_y,
        self.is_enabled = is_enabled
        self.text = title
        self.border_fg = border_fg
        self.console = tcod.console.Console(width, height, order='F')
        self.panel_z = panel_z

        self.zeroed_z_map = []
        self.z_map = []
        for map_x in range(height):
            self.zeroed_z_map.append([0 for x in range(width)])
        self.reset_z_map()

    def reset_z_map(self) -> None:
        self.z_map = [row[:] for row in self.zeroed_z_map]
