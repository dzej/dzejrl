import time


class IntervalEvent:

    def __init__(self, name, event_fn, interval=1000, **options):
        self.name = name
        self.interval = interval
        self.event_fn = event_fn
        self.options = options
        self.last_time = time.time()

    def check_for_interval(self, engine, entity, interval_evt):
        if (engine.game.curr_time - interval_evt.last_time) * 1000.0 >= interval_evt.interval:
            interval_evt.last_time = engine.game.curr_time
            self.event_fn(engine, entity, interval_evt)
