from random import randint

from dzejrl.base.types import Direction8
from dzejrl.components.drawable import DrawChar
from dzejrl.components.interval_evt import IntervalEvent
from dzejrl.engine import Engine


def random_movement(engine: Engine, entity: int, interval_evt: IntervalEvent) -> None:
    rnd = randint(1, 100)
    dr = getattr(interval_evt, 'dir', Direction8.STOP)

    if rnd <= 40:
        dr = Direction8.STOP
    if 61 <= rnd <= 80:
        dr = dr.prev() or Direction8.N
    elif 81 <= rnd:
        dr = dr.next() or Direction8.S

    interval_evt.dir = dr

    drawable = engine.world.component_for_entity(entity, DrawChar)

    if dr in [Direction8.NW, Direction8.N, Direction8.NE]:
        drawable.y -= 1
    elif dr in [Direction8.SW, Direction8.S, Direction8.SE]:
        drawable.y += 1

    if dr in [Direction8.NW, Direction8.W, Direction8.SW]:
        drawable.x -= 1
    elif dr in [Direction8.NE, Direction8.E, Direction8.SE]:
        drawable.x += 1
