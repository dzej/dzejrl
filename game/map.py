from typing import List, Optional, Type, Tuple, Dict, Union

from dzejrl.base.exceptions import GameMapError
from dzejrl.base.types import Color, FieldZPriority, MAP_Z_PRIORITY, MAP_Z_FLOOR_THICKNESS
from dzejrl.components.drawable import DrawChar
from dzejrl.components.renderable import Panel
from dzejrl.game import Game


class MapPosition:

    def __init__(self, z, y, x):
        self.x, self.y, self.z = x, y, z

    @property
    def coordinates(self) -> Tuple[int, int, int]:
        return self.z, self.y, self.x


class MapFieldType:

    def __init__(
        self,
        name: str,
        floor_type: Optional['FieldEntityType'] = None,
        *entity_types: 'FieldEntityType'
    ):
        self.name = name
        self.floor_type = floor_type
        self.entity_types = entity_types


class FieldEntityType:

    def __init__(
        self, name: str,
        is_visible: bool = True,
        allow_movement: bool = True,
        allow_vision: bool = True,
        character: Union[str, int, None] = None,
        color: Optional[Color] = None,
        z_priority: FieldZPriority = FieldZPriority.BOTTOM,
        **default_params
    ):
        self.name = name
        self.is_visible = is_visible
        self.allow_movement = allow_movement
        self.allow_vision = allow_vision
        self.character = character
        self.color = color
        self.z_priority = z_priority
        self.default_params = default_params


class FieldEntity:

    def __init__(
        self,
        game_map: 'GameMap',
        map_field: 'MapField',
        entity_type: FieldEntityType,
        is_visible: Optional[bool] = None,
        **entity_params
    ):
        self.entity_type = entity_type
        self.entity_params = entity_params
        self.drawable_cmp = None
        self.drawable_ent = None
        self.is_visible = is_visible if is_visible is not None else entity_type.is_visible

    def update_drawables(self, game_map: 'GameMap', p_pos_z: int, p_pos_y: int, p_pos_x: int):
        obj_pos_z = p_pos_z + self.entity_type.z_priority
        if (
            self.is_visible and
            0 < p_pos_x < game_map.panel.width - 1 and
            0 < p_pos_y < game_map.panel.height - 1 and
            obj_pos_z >= game_map.panel.z_map[p_pos_y][p_pos_x]
        ):
            # Entity should be visible on the screen
            if not self.drawable_cmp:
                self.drawable_cmp = DrawChar(
                    panel=game_map.panel,
                    character=self.entity_type.character,
                    x=p_pos_x, y=p_pos_y, z=obj_pos_z,
                    fg_color=self.entity_type.color
                )
                self.drawable_ent = game_map.world.create_entity(self.drawable_cmp)
            else:
                self.drawable_cmp.x = p_pos_x
                self.drawable_cmp.y = p_pos_y
                self.drawable_cmp.z = p_pos_z
        else:
            # Entity should not be visible
            if self.drawable_ent:
                game_map.world.remove_component(self.drawable_ent, DrawChar)
                self.drawable_cmp, self.drawable_ent = None, None


class MapField:

    def __init__(
        self,
        game_map: 'GameMap',
        field_type: MapFieldType,
        map_z: int, map_y: int, map_x: int
    ) -> None:

        self.field_type = field_type
        self.map_z, self.map_y, self.map_x = map_z, map_y, map_x
        self.field_entities: List[FieldEntity] = []

        if field_type.floor_type:
            self.add_field_entity(game_map, field_type.floor_type)
        for entity_type in field_type.entity_types:
            self.add_field_entity(game_map, entity_type)

    def add_field_entity(self, game_map: 'GameMap', entity_type: FieldEntityType, **entity_params):
        self.field_entities.append(
            FieldEntity(game_map=game_map, map_field=self, entity_type=entity_type, **entity_params)
        )

    def update_drawables(self, game_map: 'GameMap'):
        p_pos_z, p_pos_y, p_pos_x = self.get_panel_pos(game_map)

        for field_entity in self.field_entities:
            field_entity.update_drawables(game_map, p_pos_z, p_pos_y, p_pos_x)

    def get_panel_pos(self, game_map: 'GameMap'):
        x = self.map_x - (game_map.cam_pos.x - game_map.cam_offset_x)
        y = self.map_y - (game_map.cam_pos.y - game_map.cam_offset_y)
        z = MAP_Z_PRIORITY + self.map_z * MAP_Z_FLOOR_THICKNESS
        return z, y, x


Map3DType = List[List[List[MapField]]]


class MapGenerator:
    field_type_map: Dict[str, MapFieldType] = {}
    default_floor_tile: MapFieldType = MapFieldType('empty')

    def __init__(
        self,
        game_map: 'GameMap',
        *args, **kwargs
    ) -> None:

        self.game_map = game_map

    def get_field_type(self, character: str) -> MapFieldType:
        for symbol, field_type in self.field_type_map.items():
            if symbol == character:
                return field_type
        raise GameMapError(f'Missing MapFieldType definition for character "{character}".')

    def put_field(self, field_type: MapFieldType, map_z: int, map_y: int, map_x: int) -> bool:
        if self.game_map.is_pos_in_map(map_z, map_y, map_x):
            self.game_map.map[map_z][map_y][map_x] = MapField(self.game_map, field_type, map_z, map_y, map_x)
            return True
        else:
            return False

    def init_map(self, field_type: Optional[MapFieldType] = None) -> Map3DType:
        if field_type is None:
            field_type = self.default_floor_tile

        return [
            [
                [
                    MapField(self.game_map, field_type, z, y, x)
                    for x in range(self.game_map.width)
                ] for y in range(self.game_map.height)
            ] for z in range(self.game_map.depth)
        ]

    def put_structure(self, structure: 'MapStructure', map_pos_z: int, map_pos_y: int, map_pos_x: int):
        for curr_z, plane in enumerate(structure.definition):
            for curr_y, row in enumerate(plane.splitlines()):
                for curr_x, cell in enumerate(row):
                    self.put_field(
                        self.get_field_type(cell),
                        map_pos_z + curr_z, map_pos_y + curr_y, map_pos_x + curr_x
                    )

    def generate(self, *args, **kwargs):
        pass


class MapStructure:

    def __init__(self, *args, **kwargs):
        self.definition: List[str] = ['']

    def get_dimensions(self) -> Tuple[int, int, int]:
        depth = len(self.definition)
        height = 0
        width = 0

        for plane in self.definition:
            rows = plane.splitlines()
            height = len(rows)
            for row in rows:
                width = len(row)

        return depth, height, width


class GameMap:

    def __init__(
        self, game: Game, panel: Panel, depth: int, height: int, width: int,
        generator_cls: Optional[Type[MapGenerator]] = None,
        cam_pos: Optional[MapPosition] = None
    ) -> None:

        self.game = game
        self.panel = panel
        self.world = self.game.world
        self.width = width
        self.height = height
        self.depth = depth
        self.cam_offset_x = self.panel.width // 2
        self.cam_offset_y = self.panel.height // 2

        self.generator = generator_cls(self) if generator_cls else None

        if cam_pos:
            self.cam_pos = cam_pos
        else:
            self.cam_pos = MapPosition(
                self.depth // 2, self.height // 2, self.width // 2
            )

        self.map: Optional[Map3DType, None] = None

        if self.generator:
            self.generator.generate()

    def is_pos_in_map(self, map_z: int, map_y: int, map_x: int) -> bool:
        return (
            0 <= map_z < self.depth and
            0 <= map_y < self.height and
            0 <= map_x < self.width
        )

    def move_cam(self, dz: int = 0, dy: int = 0, dx: int = 0):
        new_z = self.cam_pos.z + dz
        new_y = self.cam_pos.y + dy
        new_x = self.cam_pos.x + dx

        if new_z < 0:
            new_z = 0
        elif new_z > self.depth - 1:
            new_z = self.depth - 1

        if new_y < 0 + self.cam_offset_y:
            new_y = 0 + self.cam_offset_y
        elif new_y > self.height - 1 - self.cam_offset_y:
            new_y = self.height - 1 - self.cam_offset_y

        if new_x < 0 + self.cam_offset_x:
            new_x = 0 + self.cam_offset_x
        elif new_x > self.width - 1 - self.cam_offset_x:
            new_x = self.width - 1 - self.cam_offset_x

        self.cam_pos.z = new_z
        self.cam_pos.y = new_y
        self.cam_pos.x = new_x

        self.refresh()

    def refresh(self):
        """
        Calculates position and state of related drawables.
        """
        for plane in self.map:
            for column in plane:
                for field in column:
                    field.update_drawables(self)
