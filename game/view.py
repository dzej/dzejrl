from typing import Type

import esper

from dzejrl.components.renderable import Panel
from dzejrl.components.drawable import Drawable


class PanelInfo:

    def __init__(self, entity: int, component: Panel):
        self.entity = entity
        self.component = component
        self.drawables = {}


class DrawableInfo:

    def __init__(self, entity: int, component: Drawable):
        self.entity = entity
        self.component = component


class GameView:

    def __init__(
        self, world: esper.World, screen_width: int, screen_height: int, is_enabled: bool = True
    ) -> None:

        self.world = world
        self.screen_width, self.screen_height = screen_width, screen_height
        self.is_enabled = is_enabled
        self.panels = {}

    def add_panel(self, name: str, x: int, y: int, width: int, height: int, panel_z: int = 0) -> Panel:
        panel = Panel(
            width=width, height=height,
            dest_x=x, dest_y=y,
            is_enabled=True,
            panel_z=panel_z
        )
        entity = self.world.create_entity(panel)
        self.panels[name] = PanelInfo(entity, panel)
        return panel

    def get_panel_cmp(self, name: str) -> Panel:
        return self.panels[name].component

    def add_drawable(self, name: str, panel_name: str, drawable_cls: Type[Drawable], **options) -> Drawable:
        drawable_cmp = drawable_cls(panel=self.get_panel_cmp(panel_name), **options)
        drawable_ent = self.world.create_entity(drawable_cmp)
        self.panels[panel_name].drawables[name] = {
            'entity': drawable_ent,
            'component': drawable_cmp
        }
        return drawable_cmp

    def enable(self) -> None:
        for key, panel_info in self.panels.items():
            panel_info.component.is_enabled = True
        self.is_enabled = True

    def disable(self) -> None:
        for key, panel_info in self.panels.items():
            panel_info.component.is_enabled = False
        self.is_enabled = False

    def destroy(self) -> None:
        for p_name, p_info in self.panels.items():
            for d_key, d_info in p_info.drawables.items():
                for cmp in self.world.components_for_entity(d_info.entity):
                    self.world.remove_component(d_info.entity, type(cmp))
            self.world.remove_component(p_info.entity, Panel)
