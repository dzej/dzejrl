from collections import OrderedDict
from enum import Enum, auto
from typing import Optional

import esper

from dzejrl.base.exceptions import GameMenuError
from dzejrl.base.types import Color, ZPosition
from dzejrl.base.utils import wrap_text
from dzejrl.components.drawable import DrawObj, DrawChar
from dzejrl.constants import COLORS
from dzejrl.drawers.frame import draw_frame
from dzejrl.drawers.text import draw_text
from dzejrl.game import GameView


class MenuItemEnum(Enum):
    NONE = auto()
    NUMBER = auto()
    LETTER = auto()


class MenuItem:
    DEFAULTS = {
        'fg_color': None,
        'bg_color': None,
        'selected_fg_color': COLORS['green'][6],
        'selected_bg_color': COLORS['black']
    }

    def __init__(
        self, menu_view: 'MenuView', token: str, text: str,
        x: int, y: int, width: int, height: int, order: int,
        **kwargs
    ) -> None:
        self.menu_view = menu_view
        self.token = token
        self.text = text
        self.x, self.y = x, y
        self.width = width
        self.order = order
        self.fg_color: Optional[Color] = kwargs.pop('fg_color', self.DEFAULTS['fg_color'])
        self.bg_color: Optional[Color] = kwargs.pop('fg_color', self.DEFAULTS['bg_color'])
        self.selected_fg_color: Optional[Color] = kwargs.pop('selected_fg_color', self.DEFAULTS['selected_fg_color'])
        self.selected_bg_color: Optional[Color] = kwargs.pop('selected_bg_color', self.DEFAULTS['selected_bg_color'])

        self.is_selected: bool = False

        menu_panel = self.menu_view.get_panel_cmp('menu')

        self.draw_cmp = DrawObj(
            panel=menu_panel,
            draw_fn=draw_text, width=width, height=height,
            x=self.x, y=self.y, z=0, text=self.text, fg_color=self.fg_color, bg_color=self.bg_color
        )
        self.entity = self.menu_view.world.create_entity(self.draw_cmp)

    def select(self) -> None:
        self.draw_cmp.fg_color = self.selected_fg_color
        self.draw_cmp.bg_color = self.selected_bg_color
        self.is_selected = True

    def deselect(self) -> None:
        self.draw_cmp.fg_color = self.fg_color
        self.draw_cmp.bg_color = self.bg_color
        self.is_selected = False


class MenuView(GameView):

    DEFAULTS = {
        'title': None,
        'max_width': None,
        'max_height': None,
        'list_offset': 2,
        'item_offset': 1,
        'enum_type': MenuItemEnum.LETTER,
        'shrink_to_items': False,
        'panel_fg': COLORS['white'],
        'panel_bg': COLORS['black'],
        'item_fg': COLORS['white'],
        'item_bg': COLORS['black'],
        'selected_fg': COLORS['green'][6],
        'selected_bg': COLORS['black'],
    }

    def __init__(
        self, world: esper.World,
        screen_width: int, screen_height: int,
        is_enabled: bool = True,
        **kwargs
    ) -> None:

        super().__init__(world, screen_width, screen_height, is_enabled)

        self.title: Optional[str] = kwargs.pop('title', self.DEFAULTS['title'])

        self.max_width = kwargs.pop('max_width', self.DEFAULTS['max_width']) or screen_width
        self.max_height = kwargs.pop('max_height', self.DEFAULTS['max_height']) or screen_height
        self.list_offset = kwargs.pop('list_offset', self.DEFAULTS['list_offset'])
        self.item_offset = kwargs.pop('item_offset', self.DEFAULTS['item_offset'])
        self.enum_type = kwargs.pop('enum_type', self.DEFAULTS['enum_type'])
        self.shrink_to_items = kwargs.pop('shrink_to_items', self.DEFAULTS['shrink_to_items'])

        self.panel_fg = kwargs.pop('panel_fg', self.DEFAULTS['panel_fg'])
        self.panel_bg = kwargs.pop('panel_bg', self.DEFAULTS['panel_bg'])
        self.item_fg = kwargs.pop('item_fg', self.DEFAULTS['item_fg'])
        self.item_bg = kwargs.pop('item_bg', self.DEFAULTS['item_bg'])
        self.selected_fg = kwargs.pop('selected_fg', self.DEFAULTS['selected_fg'])
        self.selected_bg = kwargs.pop('selected_bg', self.DEFAULTS['selected_bg'])

        self.menu_x = (self.screen_width - self.max_width) // 2
        self.menu_y = (self.screen_height - self.max_height) // 2
        self.menu_width = self.max_width

        if self.shrink_to_items:
            raise NotImplemented('shrink_to_items option has not been implemented yet.')
        else:
            self.menu_height = self.max_height

        self.new_item_y = self.menu_y + self.list_offset  # y for the new menu item - updated dynamically
        self.items = OrderedDict()
        self.selected_item = None

        # Adding basic menu entities:

        self.menu_panel = self.add_panel(
            'menu', self.menu_x, self.menu_y, self.menu_width, self.menu_height, panel_z=ZPosition.HIGH
        )
        self.add_drawable(
            name='menu_frame',
            panel_name='menu',
            drawable_cls=DrawObj,
            draw_fn=draw_frame, width=self.menu_width, height=self.menu_height,
            x=0, y=0, z=ZPosition.HIGH, text=self.title
        )
        self.item_selector_cmp = self.add_drawable(
            name='item_selector',
            panel_name='menu',
            drawable_cls=DrawChar,
            character=16, x=1, y=1, z=ZPosition.NORMAL,
            fg_color=self.selected_fg, bg_color=self.selected_bg, is_enabled=False
        )

    def select_item(self, token: str) -> MenuItem:
        try:
            new_selected_item = self.items[token]
        except KeyError:
            raise GameMenuError(f'Menu item "{token}" does not exist.')

        if self.selected_item:
            self.selected_item.deselect()
        new_selected_item.select()
        self.selected_item = new_selected_item

        self.item_selector_cmp.is_enabled = True
        self.item_selector_cmp.x = self.selected_item.draw_cmp.x - 2
        self.item_selector_cmp.y = self.selected_item.draw_cmp.y

        return self.selected_item

    def add_item(self, token: str, text: str, is_selected: bool = False) -> None:
        if token in self.items:
            raise GameMenuError(f'Item "{token}" already exists in the menu.')

        x = 4
        y = self.new_item_y

        order = len(self.items) + 1

        if self.enum_type == MenuItemEnum.NUMBER:
            text = f'{order}) {text}'
        elif self.enum_type == MenuItemEnum.LETTER:
            text = f'{chr(ord("a") + order - 1)}) {text}'

        width = self.menu_width - 6
        height = len(wrap_text(text, width))

        self.new_item_y += height + self.item_offset

        self.items[token] = MenuItem(
            menu_view=self, token=token, text=text, x=x, y=y, width=width, height=height, order=order
        )

        if is_selected:
            self.select_item(token)

    def select_next_item(self) -> None:
        if self.selected_item:
            keys = list(self.items.keys())
            new_token = self.selected_item.token  # just for fallback
            for i, token in enumerate(keys):
                if token == self.selected_item.token:
                    new_token = keys[i + 1] if i < len(keys) - 1 else keys[0]
            self.select_item(new_token)

    def select_previous_item(self) -> None:
        if self.selected_item:
            keys = list(self.items.keys())
            new_token = self.selected_item.token  # just for fallback
            for i, token in enumerate(keys):
                if token == self.selected_item.token:
                    new_token = keys[i - 1]
            self.select_item(new_token)
