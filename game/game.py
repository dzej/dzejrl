import time
from typing import Optional

import tcod
from esper import World

from dzejrl.base.input import COMMON_KEY_MAP
from dzejrl.base.types import KeyMap
from dzejrl.components.action import Action


class Game:
    title = 'Game title'

    def __init__(
        self, world: World, screen_width: int, screen_height: int
    ) -> None:

        self.screen_width, self.screen_height = screen_width, screen_height
        self.world = world
        self.curr_time = time.time()

    def update_time(self) -> None:
        self.curr_time = time.time()

    def get_key_map(self) -> KeyMap:
        return COMMON_KEY_MAP

    def decode_input(self, key: tcod.Key, mouse: tcod.Mouse, key_map: KeyMap) -> Optional[str]:
        vk = key.vk
        char = chr(key.c)
        instruction = None

        for name, conf in key_map.items():
            conf_vk, conf_char, conf_lalt = conf.get('vk'), conf.get('char'), conf.get('lalt', False)

            if conf_vk and vk == conf_vk and (key.lalt if conf_lalt else not key.lalt):
                instruction = name
                break

        return instruction

    def handle_instruction(self, instruction: str) -> None:
        """
        Override this to handle instructions in your game.
        """
        if instruction == 'set_fullscreen':
            self.world.create_entity(
                Action('set_fullscreen', params={'set': not tcod.console_is_fullscreen()})
            )

        elif instruction == 'exit_app':
            self.world.create_entity(
                Action('exit_app')
            )

    def process_input(self, key: tcod.Key, mouse: tcod.Mouse) -> None:
        """
        Executed by InputProcessor.
        """
        key_map = self.get_key_map()
        instruction = self.decode_input(key, mouse, key_map)
        self.handle_instruction(instruction)
