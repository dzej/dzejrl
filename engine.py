from typing import Type, Optional

import esper
import tcod

from dzejrl.base.exceptions import EngineConfigError
from dzejrl.base.types import CharMap
from dzejrl.game import Game
from dzejrl.processors.base import BaseProcessor
from dzejrl.processors.constants import DEFAULT_PROCESSORS_FLAG
from dzejrl.processors.mapping import DEFAULT_PROCESSORS_MAP


class Engine:

    DEFAULTS = {
        'renderer': tcod.RENDERER_OPENGL2,
        'is_full_screen': True,
        'processor_flags': DEFAULT_PROCESSORS_FLAG,
        'font_map': None
    }

    def __init__(
        self, game_cls: Type[Game], screen_width: int, screen_height: int,
        font_file: str, font_flags: int, font_cols: int, font_rows: int,
        **kwargs
    ) -> None:

        renderer = kwargs.pop('renderer', self.DEFAULTS['renderer'])
        is_full_screen = kwargs.pop('is_full_screen', self.DEFAULTS['is_full_screen'])
        processor_flags = kwargs.pop('processor_flags', self.DEFAULTS['processor_flags'])

        tcod.console_set_custom_font(font_file, font_flags, font_cols, font_rows)

        font_map = kwargs.pop('font_map', self.DEFAULTS['font_map'])
        if font_map is not None:
            self.apply_font_map(font_map)

        self.screen_width, self.screen_height = screen_width, screen_height

        self.processors = {}
        self.exit_request = False

        # ECS world instance
        self.world = esper.World()

        # Game initialization
        self.game = game_cls(self.world, self.screen_width, self.screen_height)

        self.root_console = tcod.console_init_root(
            w=screen_width, h=screen_height,
            title=self.game.title, fullscreen=is_full_screen, renderer=renderer
        )

        # ECS Initialization
        for flag, processor_cls, priority in DEFAULT_PROCESSORS_MAP:

            if processor_flags & flag:
                processor = processor_cls(engine=self)
                self.register_processor(processor_cls.__name__, processor, priority)

    def register_processor(self, name: str, processor: BaseProcessor, priority: int = 0) -> None:
        if name in self.processors.keys():
            raise EngineConfigError(f'Processor "{name}" is already registered.')
        self.processors[name] = processor
        self.world.add_processor(processor, priority)

    def apply_font_map(self, char_map: CharMap) -> None:
        for code, pos in char_map.items():
            tcod.console_map_ascii_code_to_font(code, pos[0], pos[1])

    def start(self) -> None:
        while not (tcod.console_is_window_closed() or self.exit_request):
            self.game.update_time()
            self.world.process()
